import React, { Component } from 'react';
import './Header.css';

export default class Header extends Component {

  constructor (props) {
    super(props);

    this.state = {
      text: '',
      checkAll: false
    };
  }

  checkStatus () {
    this.checkAll = this.props.items.every(el => el.done);
    console.log(this.checkAll);
  }

  toggleChange () {
    this.setState({
      checkAll: !this.checkAll
    });
  }

  render () {
    this.checkStatus();

    return (
      <div className="header">
        <div className="input-area">
          <input type="checkbox" className="check-all"
            checked={this.state.checkAll}
            onChange={this.toggleChange}
          />
          <input type={this.state.text} className="input"/>
        </div>
      </div>
    );
  }
}
