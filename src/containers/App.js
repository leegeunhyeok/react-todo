import React, { Component } from 'react';
import './App.css';

import Header from '../components/Header';

export default class App extends Component {
  render () {
    return (
      <div className="App">
        <h1 className="title">TODO</h1>
        <Header items={[{ text: 'a', done: true }, { text: 'b', done: true }]}/>
      </div>
    );
  }
}
